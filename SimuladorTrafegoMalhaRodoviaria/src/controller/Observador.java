/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.File;
import java.io.FileNotFoundException;
import model.Campo;
import model.Coordenada;

/**
 *
 * @author dougl
 */
public interface Observador {   

    public int[] retornaArrayPosicoes(File file) throws FileNotFoundException;        
    public int[] retornaLinhaColuna(File file) throws FileNotFoundException;
    public void exibirLog(String log);
    public void varrerArquivo(File file) throws FileNotFoundException;
    public void desenharExibir(String[][] dados, String colunmNames[]);
    public void liberarBotoesExecucao(boolean btnParar);
    public void notificarTabelaNovoVeiculo(Coordenada coordenada, Campo campo);

}

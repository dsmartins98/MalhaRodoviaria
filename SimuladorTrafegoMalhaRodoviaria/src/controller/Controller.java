/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import model.Campo;
import model.Coordenada;
import model.CruzamentoMonitor;
import model.CruzamentoSemaforo;
import model.InserirVeiculo;
import model.MalhaViaria;
import model.RuaMonitor;
import model.RuaSemaforo;

/**
 *
 * @author dougl
 */
public class Controller implements ControllerInterface {

    private List<Observador> observadores = new ArrayList<>();
    private MalhaViaria malhaViaria = MalhaViaria.getInstance();
    private File file;
    private int quantidadeVeiculos;
    private String modoExecucao;
    private List<Coordenada> entradasMalha;
    private InserirVeiculo inserirVeiculo = new InserirVeiculo();
    private Thread thread = new Thread(inserirVeiculo);

    @Override
    public void addObservador(Observador obs) {
        observadores.add(obs);
    }

    @Override
    public void removeObservador(Observador obs) {
        observadores.add(obs);
    }

    @Override
    public void carregarFile(File file) {

        this.file = file;

        try {

            int linhaColuna[] = null;
            for (Observador obs : observadores) {
                linhaColuna = obs.retornaLinhaColuna(file);
            }
            int linha = linhaColuna[0];
            int coluna = linhaColuna[1];

//            System.out.println("Linha: " + linha + " Coluna: " + coluna);
            malhaViaria.criarMatrizCampos(coluna, linha, modoExecucao);
            malhaViaria.setPararVeiculos(false);

            entradasMalha = new ArrayList<>();
            for (Observador obs : observadores) {
                obs.varrerArquivo(file);
            }

            String colunmNames[] = new String[coluna];
            for (int i = 0; i < colunmNames.length; i++) {
                colunmNames[i] = String.valueOf(i);
            }

            identificarCruzamentos();

            //exibir na tela
            for (Observador obs : observadores) {
                obs.desenharExibir(converterMalhaRodoviaria(), colunmNames);
                obs.liberarBotoesExecucao(true);
            }

            incluirVeiculo();
        } catch (IOException e) {
            for (Observador obs : observadores) {
                obs.exibirLog("problem accessing file" + file.getAbsolutePath());
            }
        }

    }

    @Override
    public void carregaDados(int posicoes[]) throws FileNotFoundException {

        // Início da via
        int inix = posicoes[0];
        int iniy = posicoes[1];

        // Final da via
        int fimx = posicoes[2];
        int fimy = posicoes[3];

        if (iniy == fimy) {
            if (inix < fimx) {
                for (int i = inix; i <= fimx; i++) {
                    //Verificação para não sobreescrever cruzamentos
                    if (malhaViaria.getCampos()[i][fimy] == null) {
                        if (malhaViaria.getTipoCampo().equalsIgnoreCase("Semaforo")) {
                            malhaViaria.getCampos()[i][fimy] = new RuaSemaforo("\u21d2", null, new Coordenada(i, fimy), "direita");
                        } else {
                            malhaViaria.getCampos()[i][fimy] = new RuaMonitor("\u21d2", null, new Coordenada(i, fimy), "direita");
                        }
                    } else {
                        malhaViaria.getCampos()[i][fimy].somarContadorUso();
                    }
                    identificarEntradas(new Coordenada(i, fimy));
                }
            } else {
                for (int i = inix; i >= fimx; i--) {
                    //Verificação para não sobreescrever cruzamentos
                    if (malhaViaria.getCampos()[i][fimy] == null) {
                        if (malhaViaria.getTipoCampo().equalsIgnoreCase("Semaforo")) {
                            malhaViaria.getCampos()[i][fimy] = new RuaSemaforo("\u21d0", null, new Coordenada(i, fimy), "esquerda");
                        } else {
                            malhaViaria.getCampos()[i][fimy] = new RuaMonitor("\u21d0", null, new Coordenada(i, fimy), "esquerda");
                        }
//                        malhaViaria.getCampos()[i][fimy] = new Rua("\u21d0", null, new Coordenada(i, fimy), "esquerda");
                    } else {
                        malhaViaria.getCampos()[i][fimy].somarContadorUso();
                    }
                    identificarEntradas(new Coordenada(i, fimy));
                }
            }
        } else {
            if (iniy < fimy) {
                for (int i = iniy; i <= fimy; i++) {
                    //Verificação para não sobreescrever cruzamentos
                    if (malhaViaria.getCampos()[fimx][i] == null) {
                        if (malhaViaria.getTipoCampo().equalsIgnoreCase("Semaforo")) {
                            malhaViaria.getCampos()[fimx][i] = new RuaSemaforo("\u21d3", null, new Coordenada(fimx, i), "baixo");
                        } else {
                            malhaViaria.getCampos()[fimx][i] = new RuaMonitor("\u21d3", null, new Coordenada(fimx, i), "baixo");
                        }
//                        malhaViaria.getCampos()[fimx][i] = new Rua("\u21d3", null, new Coordenada(fimx, i), "baixo");
                    } else {
                        malhaViaria.getCampos()[fimx][i].somarContadorUso();
                    }
                    identificarEntradas(new Coordenada(fimx, i));
                }
            } else {
                for (int i = iniy; i >= fimy; i--) {
                    //Verificação para não sobreescrever cruzamentos
                    if (malhaViaria.getCampos()[fimx][i] == null) {
                        if (malhaViaria.getTipoCampo().equalsIgnoreCase("Semaforo")) {
                            malhaViaria.getCampos()[fimx][i] = new RuaSemaforo("\u21d1", null, new Coordenada(fimx, i), "cima");
                        } else {
                            malhaViaria.getCampos()[fimx][i] = new RuaMonitor("\u21d1", null, new Coordenada(fimx, i), "cima");
                        }
//                        malhaViaria.getCampos()[fimx][i] = new Rua("\u21d1", null, new Coordenada(fimx, i), "cima");
                    } else {
                        malhaViaria.getCampos()[fimx][i].somarContadorUso();
                    }
                    identificarEntradas(new Coordenada(fimx, i));
                }
            }
        }
    }

    @Override
    public void setarConfiguracoes(int qtdadeVeiculos, int modoExecucao) {
        this.quantidadeVeiculos = qtdadeVeiculos;
        switch (modoExecucao) {
            case 1:
                this.modoExecucao = "semaforo";
                break;
            case 2:
                this.modoExecucao = "monitor";
                break;
            default:
                for (Observador obs : observadores) {
                    obs.exibirLog("Hey, esta opção não existe! Por favor, repita os passos anteriores.");
                }
        }
//        System.out.println("Adicionados [" + qtdadeVeiculos + "] veiculos e o modo de execução é [" + this.modoExecucao + "].");
    }

    @Override
    public void pararExecucaoMalha() {
        malhaViaria.setPararVeiculos(true);
        for (Observador obs : observadores) {
            obs.liberarBotoesExecucao(true);
        }

    }

    private void identificarEntradas(Coordenada coordenadaCampo) {

        int coordenadaX = coordenadaCampo.getX();
        int coordenadaY = coordenadaCampo.getY();

        if (((coordenadaCampo.getX() == 0 && malhaViaria.getCampos()[coordenadaX][coordenadaY].getDirecao().equalsIgnoreCase("direita"))
                || (coordenadaCampo.getY() == 0 && malhaViaria.getCampos()[coordenadaX][coordenadaY].getDirecao().equalsIgnoreCase("baixo"))
                || (coordenadaCampo.getX() == malhaViaria.getCampos().length - 1 && malhaViaria.getCampos()[coordenadaX][coordenadaY].getDirecao().equalsIgnoreCase("esquerda"))
                || (coordenadaCampo.getY() == malhaViaria.getCampos()[0].length - 1 && malhaViaria.getCampos()[coordenadaX][coordenadaY].getDirecao().equalsIgnoreCase("cima"))) && malhaViaria.getCampos()[coordenadaX][coordenadaY] != null) {
            entradasMalha.add(new Coordenada(coordenadaX, coordenadaY));
        }

    }

    private void identificarCruzamentos() {

        for (int i = 0; i < malhaViaria.getCampos().length; i++) {

            for (int k = 0; k < malhaViaria.getCampos()[0].length; k++) {

                Campo campoCarregado = malhaViaria.getCampos()[i][k];

                if (campoCarregado != null && campoCarregado.getContadorUso() > 2) {

                    int x = campoCarregado.getCoordenada().getX();
                    int y = campoCarregado.getCoordenada().getY();

                    if (malhaViaria.getTipoCampo().equalsIgnoreCase("Semaforo")) {
                        malhaViaria.getCampos()[x][y] = new CruzamentoSemaforo("\u0394", null, new Coordenada(x, y), "SemDirecao");
                    } else {

                        malhaViaria.getCampos()[x][y] = new CruzamentoMonitor("\u0394", null, new Coordenada(x, y), "SemDirecao");
                    }
                }

            }

        }

    }

    private void imprimirMatriz() {

        String matriz = "";

        for (int i = 0; i < this.malhaViaria.getCampos().length; i++) {
            for (int k = 0; k < malhaViaria.getCampos()[0].length; k++) {
                String conteudoCampo;
                try {
                    conteudoCampo = this.malhaViaria.getCampos()[k][i].getIcone();
                } catch (NullPointerException e) {
                    conteudoCampo = " ";
                }

                matriz += conteudoCampo + " ";
            }
            matriz += "\n";
        }

        System.out.println(matriz);

        System.out.println("Coordenadas das entradas da malha: ");
        String entradas = "";
        for (Coordenada c : entradasMalha) {
            entradas += "X: " + c.getX() + " Y: " + c.getY() + "\n";
        }
        System.out.println(entradas);

    }

    private void incluirVeiculo() {

        if (this.thread.isAlive()) {
            this.thread.interrupt();
        }

        this.inserirVeiculo = new InserirVeiculo(this.entradasMalha, this.quantidadeVeiculos);
        this.thread = new Thread(inserirVeiculo);
        thread.start();
    }

    @Override
    public void setarDado(Campo campo) {
        malhaViaria.getCampos()[campo.getCoordenada().getX()][campo.getCoordenada().getY()] = campo;
    }

    private String[][] converterMalhaRodoviaria() {

        int linhas = malhaViaria.getCampos().length;
        int colunas = malhaViaria.getCampos()[0].length;
        String[][] malhaString = new String[colunas][linhas];

        for (int i = 0; i < linhas; i++) {
            for (int k = 0; k < colunas; k++) {
                try {
                    malhaString[k][i] = malhaViaria.getCampos()[i][k].getIcone();
                } catch (NullPointerException e) {
                    malhaString[k][i] = " ";
                }

            }
        }

        return malhaString;

    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import model.Campo;

/**
 *
 * @author dougl
 */
public interface ControllerInterface extends Observado {
    
    public void carregarFile(File file) throws IOException;

    public void carregaDados(int posicoes[]) throws FileNotFoundException; 
    
    public void setarConfiguracoes(int qtdadeVeiculos, int modoExecucao);       
    
    public void pararExecucaoMalha();
    
    public void setarDado(Campo campo);
    
}

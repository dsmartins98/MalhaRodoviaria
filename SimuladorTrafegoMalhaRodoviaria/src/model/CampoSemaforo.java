/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author dougl
 */
public class CampoSemaforo extends Campo {

    private Semaphore mutex = new Semaphore(1);

    public CampoSemaforo(String icone, Veiculo veiculo, Coordenada coordenada, String direcao) {
        super(icone, veiculo, coordenada, direcao);
    }

    @Override
    public void setVeiculo(Veiculo veiculo) {
        try {
            mutex.acquire();
            super.setVeiculo(veiculo);
            mutex.release();
        } catch (InterruptedException ex) {            
            Logger.getLogger(CampoSemaforo.class.getName()).log(Level.SEVERE, null, ex);
        }       
    }
}

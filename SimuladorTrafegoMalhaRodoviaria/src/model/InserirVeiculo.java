/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import controller.Observador;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author dougl
 */
public class InserirVeiculo extends Thread {
    
    private List<Coordenada> entradasMalha = new ArrayList<>();
    private int quantidadeVeiculosConfigs;
    private MalhaViaria malhaViaria = MalhaViaria.getInstance();    

    public InserirVeiculo(){}
    
    public InserirVeiculo(List<Coordenada> entradasMalha, int quantidadeVeiculos) {        
        this.entradasMalha = entradasMalha;
        this.quantidadeVeiculosConfigs = quantidadeVeiculos;        
    }

    @Override
    public void run() {

        int qtdVeicTemp = quantidadeVeiculosConfigs;

        //valor aleatorio que escolherá uma entrada possível da malha
        int valorRandomico = (int) ((Math.random() * entradasMalha.size()));
        
        // Lista de posiçoes que servirão como 'indice' para o lista de entradas
        List<Integer> posicoes = new ArrayList<>();

        int cont = 0;

        //Loop que rodará enquanto o numero maximo de veiculos setado nas configurações não for atendido
        while (qtdVeicTemp > 0 && !malhaViaria.getPararVeiculos()) {

            Coordenada coordenadaEscolhida = entradasMalha.get(valorRandomico);
            
            if (malhaViaria.getCampos()[coordenadaEscolhida.getX()][coordenadaEscolhida.getY()].getVeiculo() == null) {

                int x = entradasMalha.get(valorRandomico).getX();
                int y = entradasMalha.get(valorRandomico).getY();
                
                Campo campoCarregado = malhaViaria.getCampos()[x][y];

                if (campoCarregado.getVeiculo() == null) {
                    
                    Veiculo veiculo = new Veiculo(cont, campoCarregado);
                    campoCarregado.setIcone("\u058e");
                    campoCarregado.setVeiculo(veiculo);                    
                    malhaViaria.getCampos()[x][y] = campoCarregado;
                    malhaViaria.notificarTabelaNovoVeiculo(new Coordenada(x, y), campoCarregado);
                    cont++;
                    posicoes.add(valorRandomico);
                    qtdVeicTemp--;
                    
                    // dar comando para veiculo andar
                    Thread threadVeiculo = new Thread(veiculo);
                    threadVeiculo.start();
                    malhaViaria.addVeiculosAtivos(threadVeiculo);                    
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException ex) {                        
                        Logger.getLogger(InserirVeiculo.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }

            }

            valorRandomico = (int) ((Math.random() * entradasMalha.size()));

        }

    }

}

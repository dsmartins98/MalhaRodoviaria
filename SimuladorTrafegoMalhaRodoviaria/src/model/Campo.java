/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

public abstract class Campo {

    private String icone;
    private String iconeFinal;
    private Veiculo veiculo;
    private Coordenada coordenada;
    private String direcao;
    private int contadorUso = 0;

    public Campo(String icone, Veiculo veiculo, Coordenada coordenada, String direcao) {
        this.icone = icone;
        this.veiculo = veiculo;
        this.coordenada = coordenada;
        this.direcao = direcao;
        this.iconeFinal = icone;
        contadorUso++;
    }

    public String getIcone() {
        return icone;
    }

    public void setIcone(String icone) {
        this.icone = icone;
    }

    public String getIconeFinal() {
        return iconeFinal;
    }

    public void setIconeFinal(String iconeFinal) {
        this.iconeFinal = iconeFinal;
    }

    public Veiculo getVeiculo() {
        return veiculo;
    }
    
    public void setVeiculo(Veiculo veiculo) {
        this.veiculo = veiculo;
    }

    public Coordenada getCoordenada() {
        return coordenada;
    }

    public void setCoordenada(Coordenada coordenada) {
        this.coordenada = coordenada;
    }

    public String getDirecao() {
        return direcao;
    }

    public void setDirecao(String direcao) {
        this.direcao = direcao;
    }

    public int getContadorUso() {
        return contadorUso;
    }

    public void setContadorUso(int contadorUso) {
        this.contadorUso = contadorUso;
    }

    public void somarContadorUso() {
        this.contadorUso++;
    }

}

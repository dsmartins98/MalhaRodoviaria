package model;

import controller.Observado;
import controller.Observador;
import java.util.ArrayList;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author dougl
 */
public class MalhaViaria implements Observado {

    private Campo[][] campos;
    private static MalhaViaria instance;
    private List<Observador> observadores = new ArrayList<>();
    private String tipoCampo;
    private List<Thread> veiculosAtivos = new ArrayList<>();
    private boolean pararVeiculos;

    private MalhaViaria() {
    }

    public synchronized static MalhaViaria getInstance() {

        if (instance == null) {
            instance = new MalhaViaria();
        }

        return instance;
    }
    
    public void addVeiculosAtivos(Thread thread){
        veiculosAtivos.add(thread);
    }

    public List<Thread> getVeiculosAtivos() {
        return veiculosAtivos;
    }        

    public void criarMatrizCampos(int x, int y, String tipo) {
        this.tipoCampo = tipo;
        if (tipo.equalsIgnoreCase("semaforo")) {
            campos = new CampoSemaforo[x][y];
        } else {
            campos = new CampoMonitor[x][y];
        }

    }

    public Campo[][] getCampos() {
        return campos;
    }

    public void setCampos(Campo[][] campos) {
        this.campos = campos;
    }

    @Override
    public void addObservador(Observador obs) {
        this.observadores.add(obs);
    }

    @Override
    public void removeObservador(Observador obs) {
        this.observadores.remove(obs);
    }

    public void notificarTabelaNovoVeiculo(Coordenada coordenada, Campo campo) {
        for (Observador obs : observadores) {
            obs.notificarTabelaNovoVeiculo(coordenada, campo);
        }
    }

    public String getTipoCampo() {
        return tipoCampo;
    }

    public void setTipoCampo(String tipoCampo) {
        this.tipoCampo = tipoCampo;
    }

    public boolean getPararVeiculos() {
        return pararVeiculos;
    }

    public void setPararVeiculos(boolean pararVeiculos) {
        this.pararVeiculos = pararVeiculos;
    }
    
    

}

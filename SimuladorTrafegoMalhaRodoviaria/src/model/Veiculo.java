/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author dougl
 */
public class Veiculo implements Runnable {

    private int id;
    private Campo campo;
    private MalhaViaria malhaViaria = MalhaViaria.getInstance();

    public Veiculo(int id, Campo campo) {
        this.id = id;
        this.campo = campo;
    }

    @Override
    public void run() {
        
        int tempoAleatorio = (int) (Math.random() * 2000);
        boolean saida = false;

        while (!malhaViaria.getPararVeiculos() && !saida) {

            //aqui verifico a direção da via
            //consequentemente já sei que é uma rua (pois há direção)
            String direcao = campo.getDirecao();
            Campo campoAntigo;
            Campo proximoCampo;

            switch (direcao) {
                case "direita":

                    campoAntigo = campo;
                    try {
                        proximoCampo = malhaViaria.getCampos()[campo.getCoordenada().getX() + 1][campo.getCoordenada().getY()];

                        // Verifico se a posição a frente está disponivel para avançar
                        if (proximoCampo.getVeiculo() == null) {

                            //verifica se o campo na frente do carro é um campo do tipo Rua
                            if (proximoCampo instanceof RuaSemaforo || proximoCampo instanceof RuaMonitor) {

                                /*
                            * Primeiramente é setado o veiculo no proximo campo.
                            * Depois o atributo 'campo' de veiculo é atualizado.
                            * Em seguida, é atualizada a Malha Viaria com as devidas alterações
                                 */
                                proximoCampo.setVeiculo(this);
                                proximoCampo.setIcone("\u058e");
                                this.campo = proximoCampo;
                                malhaViaria.getCampos()[proximoCampo.getCoordenada().getX()][proximoCampo.getCoordenada().getY()] = proximoCampo;
                                malhaViaria.getCampos()[campoAntigo.getCoordenada().getX()][campoAntigo.getCoordenada().getY()].setVeiculo(null);
                                malhaViaria.getCampos()[campoAntigo.getCoordenada().getX()][campoAntigo.getCoordenada().getY()].setIcone(campoAntigo.getIconeFinal());

                                //atualiza em tela os dois campos modificados
                                malhaViaria.notificarTabelaNovoVeiculo(new Coordenada(campoAntigo.getCoordenada().getX(), campoAntigo.getCoordenada().getY()), campoAntigo);
                                malhaViaria.notificarTabelaNovoVeiculo(new Coordenada(proximoCampo.getCoordenada().getX(), proximoCampo.getCoordenada().getY()), this.campo);

                            } else {

                                /* Verificar para onde ele vai */
                                List<Coordenada> caminhosPossivels = retornaCaminhosPossiveis(this);
                                //Gera um valor aleatório
                                int valorRandomico = (int) ((Math.random() * caminhosPossivels.size()));
                                //Captura a coordenada escolhida através do valor aleatório
                                Coordenada coordenadaEscolhida = caminhosPossivels.get(valorRandomico);

                                if (malhaViaria.getCampos()[coordenadaEscolhida.getX()][coordenadaEscolhida.getY()].getVeiculo() == null) {

                                    // [INICIO] - movimenta para cruzamento                                
                                    proximoCampo.setVeiculo(this);
                                    proximoCampo.setIcone("\u058e");
                                    this.campo = proximoCampo;
                                    malhaViaria.getCampos()[proximoCampo.getCoordenada().getX()][proximoCampo.getCoordenada().getY()] = proximoCampo;
                                    malhaViaria.getCampos()[campoAntigo.getCoordenada().getX()][campoAntigo.getCoordenada().getY()].setVeiculo(null);
                                    malhaViaria.getCampos()[campoAntigo.getCoordenada().getX()][campoAntigo.getCoordenada().getY()].setIcone(campoAntigo.getIconeFinal());

                                    //atualiza em tela os dois campos modificados
                                    malhaViaria.notificarTabelaNovoVeiculo(new Coordenada(campoAntigo.getCoordenada().getX(), campoAntigo.getCoordenada().getY()), campoAntigo);
                                    malhaViaria.notificarTabelaNovoVeiculo(new Coordenada(proximoCampo.getCoordenada().getX(), proximoCampo.getCoordenada().getY()), this.campo);
                                    // [FIM] - movimenta para cruzamento                                                               

                                    try {
                                        Thread.sleep(500);
                                    } catch (InterruptedException ex) {
                                        Logger.getLogger(Veiculo.class.getName()).log(Level.SEVERE, null, ex);
                                    }

                                    // [INICIO] - movimenta para rua escolhida
                                    campoAntigo = this.campo;

                                    //...
                                    malhaViaria.getCampos()[coordenadaEscolhida.getX()][coordenadaEscolhida.getY()].setVeiculo(this);
                                    malhaViaria.getCampos()[coordenadaEscolhida.getX()][coordenadaEscolhida.getY()].setIcone("\u058e");

                                    //Atualiza campos antigos                            
                                    malhaViaria.getCampos()[this.campo.getCoordenada().getX()][this.campo.getCoordenada().getY()].setVeiculo(null);
                                    malhaViaria.getCampos()[this.campo.getCoordenada().getX()][this.campo.getCoordenada().getY()].setIcone(malhaViaria.getCampos()[this.campo.getCoordenada().getX()][this.campo.getCoordenada().getY()].getIconeFinal());

                                    //Atribui a coordenada escolhida o veiculo em questão                            
                                    malhaViaria.getCampos()[coordenadaEscolhida.getX()][coordenadaEscolhida.getY()].getVeiculo().setCampo(malhaViaria.getCampos()[coordenadaEscolhida.getX()][coordenadaEscolhida.getY()]);

                                    //Atualiza em tela os dois campos modificados
                                    malhaViaria.notificarTabelaNovoVeiculo(new Coordenada(campoAntigo.getCoordenada().getX(), campoAntigo.getCoordenada().getY()), campoAntigo);
                                    malhaViaria.notificarTabelaNovoVeiculo(new Coordenada(coordenadaEscolhida.getX(), coordenadaEscolhida.getY()), this.campo);

                                }

                            }
                        }
                    } catch (ArrayIndexOutOfBoundsException e) {
                        malhaViaria.getCampos()[this.campo.getCoordenada().getX()][this.campo.getCoordenada().getY()].setIcone(malhaViaria.getCampos()[this.campo.getCoordenada().getX()][this.campo.getCoordenada().getY()].getIconeFinal());
                        malhaViaria.getCampos()[this.campo.getCoordenada().getX()][this.campo.getCoordenada().getY()].setVeiculo(null);
                        //Atualiza em tela os dois campos modificados
                        malhaViaria.notificarTabelaNovoVeiculo(new Coordenada(this.campo.getCoordenada().getX(), this.campo.getCoordenada().getX()), malhaViaria.getCampos()[this.campo.getCoordenada().getX()][this.campo.getCoordenada().getY()]);
                        malhaViaria.notificarTabelaNovoVeiculo(new Coordenada(this.campo.getCoordenada().getX(), this.campo.getCoordenada().getY()), malhaViaria.getCampos()[this.campo.getCoordenada().getX()][this.campo.getCoordenada().getY()]);
                        saida = true;
                    }

                    break;
                case "esquerda":

                    campoAntigo = campo;
                    try {
                        proximoCampo = malhaViaria.getCampos()[campo.getCoordenada().getX() - 1][campo.getCoordenada().getY()];

                        // Verifico se a posição a frente está disponivel para avançar
                        if (proximoCampo.getVeiculo() == null) {
                            if (proximoCampo instanceof RuaSemaforo || proximoCampo instanceof RuaMonitor) {
                                /*
                    * Primeiramente é setado o veiculo no proximo campo.
                    * Depois o atributo 'campo' de veiculo é atualizado.
                    * Em seguida, é atualizada a Malha Viaria com as devidas alterações
                                 */
                                proximoCampo.setVeiculo(this);
                                proximoCampo.setIcone("\u058e");
                                this.campo = proximoCampo;
                                malhaViaria.getCampos()[proximoCampo.getCoordenada().getX()][proximoCampo.getCoordenada().getY()] = proximoCampo;
                                malhaViaria.getCampos()[campoAntigo.getCoordenada().getX()][campoAntigo.getCoordenada().getY()].setVeiculo(null);
                                malhaViaria.getCampos()[campoAntigo.getCoordenada().getX()][campoAntigo.getCoordenada().getY()].setIcone(campoAntigo.getIconeFinal());

                                //atualiza em tela os dois campos modificados
                                malhaViaria.notificarTabelaNovoVeiculo(new Coordenada(campoAntigo.getCoordenada().getX(), campoAntigo.getCoordenada().getY()), campoAntigo);
                                malhaViaria.notificarTabelaNovoVeiculo(new Coordenada(proximoCampo.getCoordenada().getX(), proximoCampo.getCoordenada().getY()), this.campo);
//                        System.out.println("Veiculo mudou para posição: X" + proximoCampo.getCoordenada().getX() + " Y: " + proximoCampo.getCoordenada().getY());

                            } else {

                                /* Verificar para onde ele vai */
                                List<Coordenada> caminhosPossivels = retornaCaminhosPossiveis(this);
                                //Gera um valor aleatório
                                int valorRandomico = (int) ((Math.random() * caminhosPossivels.size()));
                                //Captura a coordenada escolhida através do valor aleatório
                                Coordenada coordenadaEscolhida = caminhosPossivels.get(valorRandomico);

                                if (malhaViaria.getCampos()[coordenadaEscolhida.getX()][coordenadaEscolhida.getY()].getVeiculo() == null) {

                                    // [INICIO] - movimenta para cruzamento                                
                                    proximoCampo.setVeiculo(this);
                                    proximoCampo.setIcone("\u058e");
                                    this.campo = proximoCampo;
                                    malhaViaria.getCampos()[proximoCampo.getCoordenada().getX()][proximoCampo.getCoordenada().getY()] = proximoCampo;
                                    malhaViaria.getCampos()[campoAntigo.getCoordenada().getX()][campoAntigo.getCoordenada().getY()].setVeiculo(null);
                                    malhaViaria.getCampos()[campoAntigo.getCoordenada().getX()][campoAntigo.getCoordenada().getY()].setIcone(campoAntigo.getIconeFinal());

                                    //atualiza em tela os dois campos modificados
                                    malhaViaria.notificarTabelaNovoVeiculo(new Coordenada(campoAntigo.getCoordenada().getX(), campoAntigo.getCoordenada().getY()), campoAntigo);
                                    malhaViaria.notificarTabelaNovoVeiculo(new Coordenada(proximoCampo.getCoordenada().getX(), proximoCampo.getCoordenada().getY()), this.campo);
                                    // [FIM] - movimenta para cruzamento                                                               

                                    try {
                                        Thread.sleep(500);
                                    } catch (InterruptedException ex) {
                                        Logger.getLogger(Veiculo.class.getName()).log(Level.SEVERE, null, ex);
                                    }

                                    // [INICIO] - movimenta para rua escolhida
                                    campoAntigo = this.campo;

                                    //...
                                    malhaViaria.getCampos()[coordenadaEscolhida.getX()][coordenadaEscolhida.getY()].setVeiculo(this);
                                    malhaViaria.getCampos()[coordenadaEscolhida.getX()][coordenadaEscolhida.getY()].setIcone("\u058e");

                                    //Atualiza campos antigos                            
                                    malhaViaria.getCampos()[this.campo.getCoordenada().getX()][this.campo.getCoordenada().getY()].setVeiculo(null);
                                    malhaViaria.getCampos()[this.campo.getCoordenada().getX()][this.campo.getCoordenada().getY()].setIcone(malhaViaria.getCampos()[this.campo.getCoordenada().getX()][this.campo.getCoordenada().getY()].getIconeFinal());

                                    //Atribui a coordenada escolhida o veiculo em questão                            
                                    malhaViaria.getCampos()[coordenadaEscolhida.getX()][coordenadaEscolhida.getY()].getVeiculo().setCampo(malhaViaria.getCampos()[coordenadaEscolhida.getX()][coordenadaEscolhida.getY()]);

                                    //Atualiza em tela os dois campos modificados
                                    malhaViaria.notificarTabelaNovoVeiculo(new Coordenada(campoAntigo.getCoordenada().getX(), campoAntigo.getCoordenada().getY()), campoAntigo);
                                    malhaViaria.notificarTabelaNovoVeiculo(new Coordenada(coordenadaEscolhida.getX(), coordenadaEscolhida.getY()), this.campo);

                                }

                            }
                        }
                    } catch (ArrayIndexOutOfBoundsException e) {
                        System.out.println("Entrou no de direita. X: " + this.campo.getCoordenada().getX() + " / Y: " + this.campo.getCoordenada().getY());
                        malhaViaria.getCampos()[this.campo.getCoordenada().getX()][this.campo.getCoordenada().getY()].setIcone(malhaViaria.getCampos()[this.campo.getCoordenada().getX()][this.campo.getCoordenada().getY()].getIconeFinal());
                        malhaViaria.getCampos()[this.campo.getCoordenada().getX()][this.campo.getCoordenada().getY()].setVeiculo(null);
                        //Atualiza em tela os dois campos modificados
                        malhaViaria.notificarTabelaNovoVeiculo(new Coordenada(this.campo.getCoordenada().getX(), this.campo.getCoordenada().getX()), malhaViaria.getCampos()[this.campo.getCoordenada().getX()][this.campo.getCoordenada().getY()]);
                        malhaViaria.notificarTabelaNovoVeiculo(new Coordenada(this.campo.getCoordenada().getX(), this.campo.getCoordenada().getY()), malhaViaria.getCampos()[this.campo.getCoordenada().getX()][this.campo.getCoordenada().getY()]);
                        saida = true;
                    }

                    break;

                case "baixo":

                    campoAntigo = campo;

                    try {

                        proximoCampo = malhaViaria.getCampos()[campo.getCoordenada().getX()][campo.getCoordenada().getY() + 1];

                        // Verifico se a posição a frente está disponivel para avançar
                        if (proximoCampo.getVeiculo() == null) {

                            if (proximoCampo instanceof RuaSemaforo || proximoCampo instanceof RuaMonitor) {


                                /*
                    * Primeiramente é setado o veiculo no proximo campo.
                    * Depois o atributo 'campo' de veiculo é atualizado.
                    * Em seguida, é atualizada a Malha Viaria com as devidas alterações
                                 */
                                proximoCampo.setVeiculo(this);
                                proximoCampo.setIcone("\u058e");
                                this.campo = proximoCampo;
                                malhaViaria.getCampos()[proximoCampo.getCoordenada().getX()][proximoCampo.getCoordenada().getY()] = proximoCampo;
                                malhaViaria.getCampos()[campoAntigo.getCoordenada().getX()][campoAntigo.getCoordenada().getY()].setVeiculo(null);
                                malhaViaria.getCampos()[campoAntigo.getCoordenada().getX()][campoAntigo.getCoordenada().getY()].setIcone(campoAntigo.getIconeFinal());

                                //atualiza em tela os dois campos modificados
                                malhaViaria.notificarTabelaNovoVeiculo(new Coordenada(campoAntigo.getCoordenada().getX(), campoAntigo.getCoordenada().getY()), campoAntigo);
                                malhaViaria.notificarTabelaNovoVeiculo(new Coordenada(proximoCampo.getCoordenada().getX(), proximoCampo.getCoordenada().getY()), this.campo);
                            } else {

                                /* Verificar para onde ele vai */
                                List<Coordenada> caminhosPossivels = retornaCaminhosPossiveis(this);
                                //Gera um valor aleatório
                                int valorRandomico = (int) ((Math.random() * caminhosPossivels.size()));
                                //Captura a coordenada escolhida através do valor aleatório
                                Coordenada coordenadaEscolhida = caminhosPossivels.get(valorRandomico);

                                if (malhaViaria.getCampos()[coordenadaEscolhida.getX()][coordenadaEscolhida.getY()].getVeiculo() == null) {

                                    // [INICIO] - movimenta para cruzamento                                
                                    proximoCampo.setVeiculo(this);
                                    proximoCampo.setIcone("\u058e");
                                    this.campo = proximoCampo;
                                    malhaViaria.getCampos()[proximoCampo.getCoordenada().getX()][proximoCampo.getCoordenada().getY()] = proximoCampo;
                                    malhaViaria.getCampos()[campoAntigo.getCoordenada().getX()][campoAntigo.getCoordenada().getY()].setVeiculo(null);
                                    malhaViaria.getCampos()[campoAntigo.getCoordenada().getX()][campoAntigo.getCoordenada().getY()].setIcone(campoAntigo.getIconeFinal());

                                    //atualiza em tela os dois campos modificados
                                    malhaViaria.notificarTabelaNovoVeiculo(new Coordenada(campoAntigo.getCoordenada().getX(), campoAntigo.getCoordenada().getY()), campoAntigo);
                                    malhaViaria.notificarTabelaNovoVeiculo(new Coordenada(proximoCampo.getCoordenada().getX(), proximoCampo.getCoordenada().getY()), this.campo);
                                    // [FIM] - movimenta para cruzamento                                                               

                                    try {
                                        Thread.sleep(500);
                                    } catch (InterruptedException ex) {
                                        Logger.getLogger(Veiculo.class.getName()).log(Level.SEVERE, null, ex);
                                    }

                                    // [INICIO] - movimenta para rua escolhida
                                    campoAntigo = this.campo;

                                    //...
                                    malhaViaria.getCampos()[coordenadaEscolhida.getX()][coordenadaEscolhida.getY()].setVeiculo(this);
                                    malhaViaria.getCampos()[coordenadaEscolhida.getX()][coordenadaEscolhida.getY()].setIcone("\u058e");

                                    //Atualiza campos antigos                            
                                    malhaViaria.getCampos()[this.campo.getCoordenada().getX()][this.campo.getCoordenada().getY()].setVeiculo(null);
                                    malhaViaria.getCampos()[this.campo.getCoordenada().getX()][this.campo.getCoordenada().getY()].setIcone(malhaViaria.getCampos()[this.campo.getCoordenada().getX()][this.campo.getCoordenada().getY()].getIconeFinal());

                                    //Atribui a coordenada escolhida o veiculo em questão                            
                                    malhaViaria.getCampos()[coordenadaEscolhida.getX()][coordenadaEscolhida.getY()].getVeiculo().setCampo(malhaViaria.getCampos()[coordenadaEscolhida.getX()][coordenadaEscolhida.getY()]);

                                    //Atualiza em tela os dois campos modificados
                                    malhaViaria.notificarTabelaNovoVeiculo(new Coordenada(campoAntigo.getCoordenada().getX(), campoAntigo.getCoordenada().getY()), campoAntigo);
                                    malhaViaria.notificarTabelaNovoVeiculo(new Coordenada(coordenadaEscolhida.getX(), coordenadaEscolhida.getY()), this.campo);

                                }

                            }

                        }
                    } catch (ArrayIndexOutOfBoundsException e) {
                        malhaViaria.getCampos()[this.campo.getCoordenada().getX()][this.campo.getCoordenada().getY()].setIcone(malhaViaria.getCampos()[this.campo.getCoordenada().getX()][this.campo.getCoordenada().getY()].getIconeFinal());
                        malhaViaria.getCampos()[this.campo.getCoordenada().getX()][this.campo.getCoordenada().getY()].setVeiculo(null);
                        //Atualiza em tela os dois campos modificados
                        malhaViaria.notificarTabelaNovoVeiculo(new Coordenada(this.campo.getCoordenada().getX(), this.campo.getCoordenada().getX()), malhaViaria.getCampos()[this.campo.getCoordenada().getX()][this.campo.getCoordenada().getY()]);
                        malhaViaria.notificarTabelaNovoVeiculo(new Coordenada(this.campo.getCoordenada().getX(), this.campo.getCoordenada().getY()), malhaViaria.getCampos()[this.campo.getCoordenada().getX()][this.campo.getCoordenada().getY()]);
                        saida = true;
                    }

                    break;
                case "cima":

                    campoAntigo = campo;

                    try {
                        proximoCampo = malhaViaria.getCampos()[campo.getCoordenada().getX()][campo.getCoordenada().getY() - 1];

                        if (proximoCampo.getVeiculo() == null) {
                            if (proximoCampo instanceof RuaSemaforo || proximoCampo instanceof RuaMonitor) {

                                // Verifico se a posição a frente está disponivel para avançar

                                /*
                    * Primeiramente é setado o veiculo no proximo campo.
                    * Depois o atributo 'campo' de veiculo é atualizado.
                    * Em seguida, é atualizada a Malha Viaria com as devidas alterações
                                 */
                                proximoCampo.setVeiculo(this);
                                proximoCampo.setIcone("\u058e");
                                this.campo = proximoCampo;
                                malhaViaria.getCampos()[proximoCampo.getCoordenada().getX()][proximoCampo.getCoordenada().getY()] = proximoCampo;
                                malhaViaria.getCampos()[campoAntigo.getCoordenada().getX()][campoAntigo.getCoordenada().getY()].setVeiculo(null);
                                malhaViaria.getCampos()[campoAntigo.getCoordenada().getX()][campoAntigo.getCoordenada().getY()].setIcone(campoAntigo.getIconeFinal());

                                //atualiza em tela os dois campos modificados
                                malhaViaria.notificarTabelaNovoVeiculo(new Coordenada(campoAntigo.getCoordenada().getX(), campoAntigo.getCoordenada().getY()), campoAntigo);
                                malhaViaria.notificarTabelaNovoVeiculo(new Coordenada(proximoCampo.getCoordenada().getX(), proximoCampo.getCoordenada().getY()), this.campo);
                            } else {

                                /* Verificar para onde ele vai */
                                List<Coordenada> caminhosPossivels = retornaCaminhosPossiveis(this);
                                //Gera um valor aleatório
                                int valorRandomico = (int) ((Math.random() * caminhosPossivels.size()));
                                //Captura a coordenada escolhida através do valor aleatório
                                Coordenada coordenadaEscolhida = caminhosPossivels.get(valorRandomico);

                                if (malhaViaria.getCampos()[coordenadaEscolhida.getX()][coordenadaEscolhida.getY()].getVeiculo() == null) {

                                    // [INICIO] - movimenta para cruzamento                                
                                    proximoCampo.setVeiculo(this);
                                    proximoCampo.setIcone("\u058e");
                                    this.campo = proximoCampo;
                                    malhaViaria.getCampos()[proximoCampo.getCoordenada().getX()][proximoCampo.getCoordenada().getY()] = proximoCampo;
                                    malhaViaria.getCampos()[campoAntigo.getCoordenada().getX()][campoAntigo.getCoordenada().getY()].setVeiculo(null);
                                    malhaViaria.getCampos()[campoAntigo.getCoordenada().getX()][campoAntigo.getCoordenada().getY()].setIcone(campoAntigo.getIconeFinal());

                                    //atualiza em tela os dois campos modificados
                                    malhaViaria.notificarTabelaNovoVeiculo(new Coordenada(campoAntigo.getCoordenada().getX(), campoAntigo.getCoordenada().getY()), campoAntigo);
                                    malhaViaria.notificarTabelaNovoVeiculo(new Coordenada(proximoCampo.getCoordenada().getX(), proximoCampo.getCoordenada().getY()), this.campo);
                                    // [FIM] - movimenta para cruzamento                                                               

                                    try {
                                        Thread.sleep(500);
                                    } catch (InterruptedException ex) {
                                        Logger.getLogger(Veiculo.class.getName()).log(Level.SEVERE, null, ex);
                                    }

                                    // [INICIO] - movimenta para rua escolhida
                                    campoAntigo = this.campo;

                                    //...
                                    malhaViaria.getCampos()[coordenadaEscolhida.getX()][coordenadaEscolhida.getY()].setVeiculo(this);
                                    malhaViaria.getCampos()[coordenadaEscolhida.getX()][coordenadaEscolhida.getY()].setIcone("\u058e");

                                    //Atualiza campos antigos                            
                                    malhaViaria.getCampos()[this.campo.getCoordenada().getX()][this.campo.getCoordenada().getY()].setVeiculo(null);
                                    malhaViaria.getCampos()[this.campo.getCoordenada().getX()][this.campo.getCoordenada().getY()].setIcone(malhaViaria.getCampos()[this.campo.getCoordenada().getX()][this.campo.getCoordenada().getY()].getIconeFinal());

                                    //Atribui a coordenada escolhida o veiculo em questão                            
                                    malhaViaria.getCampos()[coordenadaEscolhida.getX()][coordenadaEscolhida.getY()].getVeiculo().setCampo(malhaViaria.getCampos()[coordenadaEscolhida.getX()][coordenadaEscolhida.getY()]);

                                    //Atualiza em tela os dois campos modificados
                                    malhaViaria.notificarTabelaNovoVeiculo(new Coordenada(campoAntigo.getCoordenada().getX(), campoAntigo.getCoordenada().getY()), campoAntigo);
                                    malhaViaria.notificarTabelaNovoVeiculo(new Coordenada(coordenadaEscolhida.getX(), coordenadaEscolhida.getY()), this.campo);

                                }

                            }

                        }
                    } catch (ArrayIndexOutOfBoundsException e) {
                        malhaViaria.getCampos()[this.campo.getCoordenada().getX()][this.campo.getCoordenada().getY()].setIcone(malhaViaria.getCampos()[this.campo.getCoordenada().getX()][this.campo.getCoordenada().getY()].getIconeFinal());
                        malhaViaria.getCampos()[this.campo.getCoordenada().getX()][this.campo.getCoordenada().getY()].setVeiculo(null);
                        //Atualiza em tela os dois campos modificados
                        malhaViaria.notificarTabelaNovoVeiculo(new Coordenada(this.campo.getCoordenada().getX(), this.campo.getCoordenada().getX()), malhaViaria.getCampos()[this.campo.getCoordenada().getX()][this.campo.getCoordenada().getY()]);
                        malhaViaria.notificarTabelaNovoVeiculo(new Coordenada(this.campo.getCoordenada().getX(), this.campo.getCoordenada().getY()), malhaViaria.getCampos()[this.campo.getCoordenada().getX()][this.campo.getCoordenada().getY()]);
                        saida = true;
                    }

                    break;
                default:
                    saida = true;
            }
                        
            try {
                Thread.sleep(tempoAleatorio);
            } catch (InterruptedException ex) {
                Logger.getLogger(Veiculo.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private List<Coordenada> retornaCaminhosPossiveis(Veiculo veiculo) {
        List<Coordenada> caminhosPossivels = new ArrayList<>();

        Coordenada coordenadaAtualVeiculo = new Coordenada(veiculo.getCampo().getCoordenada().getX(), veiculo.getCampo().getCoordenada().getY());
        String direcaoAtualVeiculo = veiculo.getCampo().getDirecao();

        switch (direcaoAtualVeiculo) {
            case "direita":

                //verificando em cima do cruzamento a frente
                if ((malhaViaria.getCampos()[coordenadaAtualVeiculo.getX() + 1][coordenadaAtualVeiculo.getY() - 1] != null)
                        && (malhaViaria.getCampos()[coordenadaAtualVeiculo.getX() + 1][coordenadaAtualVeiculo.getY() - 1].getDirecao().equalsIgnoreCase("cima"))) {

                    caminhosPossivels.add(new Coordenada(coordenadaAtualVeiculo.getX() + 1, coordenadaAtualVeiculo.getY() - 1));

                }
                //verificando na frente do cruzamento a frente
                if ((malhaViaria.getCampos()[coordenadaAtualVeiculo.getX() + 2][coordenadaAtualVeiculo.getY()] != null)
                        && (malhaViaria.getCampos()[coordenadaAtualVeiculo.getX() + 2][coordenadaAtualVeiculo.getY()].getDirecao().equalsIgnoreCase("direita"))) {
                    caminhosPossivels.add((new Coordenada(coordenadaAtualVeiculo.getX() + 2, coordenadaAtualVeiculo.getY())));
                }
                //verificando em baixo do cruzamento a frente
                if ((malhaViaria.getCampos()[coordenadaAtualVeiculo.getX() + 1][coordenadaAtualVeiculo.getY() + 1] != null)
                        && (malhaViaria.getCampos()[coordenadaAtualVeiculo.getX() + 1][coordenadaAtualVeiculo.getY() + 1].getDirecao().equalsIgnoreCase("baixo"))) {
                    caminhosPossivels.add((new Coordenada(coordenadaAtualVeiculo.getX() + 1, coordenadaAtualVeiculo.getY() + 1)));
                }

                break;
            case "esquerda":
                //verificando em cima do cruzamento a frente
                if ((malhaViaria.getCampos()[coordenadaAtualVeiculo.getX() - 1][coordenadaAtualVeiculo.getY() - 1] != null)
                        && (malhaViaria.getCampos()[coordenadaAtualVeiculo.getX() - 1][coordenadaAtualVeiculo.getY() - 1].getDirecao().equalsIgnoreCase("cima"))) {

                    caminhosPossivels.add(new Coordenada(coordenadaAtualVeiculo.getX() - 1, coordenadaAtualVeiculo.getY() - 1));

                }
                //verificando na frente do cruzamento a frente
                if ((malhaViaria.getCampos()[coordenadaAtualVeiculo.getX() - 2][coordenadaAtualVeiculo.getY()] != null)
                        && (malhaViaria.getCampos()[coordenadaAtualVeiculo.getX() - 2][coordenadaAtualVeiculo.getY()].getDirecao().equalsIgnoreCase("esquerda"))) {

                    caminhosPossivels.add((new Coordenada(coordenadaAtualVeiculo.getX() - 2, coordenadaAtualVeiculo.getY())));

                }
                //verificando em baixo do cruzamento a frente
                if ((malhaViaria.getCampos()[coordenadaAtualVeiculo.getX() - 1][coordenadaAtualVeiculo.getY() + 1] != null)
                        && (malhaViaria.getCampos()[coordenadaAtualVeiculo.getX() - 1][coordenadaAtualVeiculo.getY() + 1].getDirecao().equalsIgnoreCase("baixo"))) {

                    caminhosPossivels.add((new Coordenada(coordenadaAtualVeiculo.getX() - 1, coordenadaAtualVeiculo.getY() + 1)));

                }

                break;
            case "cima":

                //verificando na esquerda do cruzamento a frente
                if ((malhaViaria.getCampos()[coordenadaAtualVeiculo.getX() - 1][coordenadaAtualVeiculo.getY() - 1] != null)
                        && (malhaViaria.getCampos()[coordenadaAtualVeiculo.getX() - 1][coordenadaAtualVeiculo.getY() - 1].getDirecao().equalsIgnoreCase("esquerda"))) {

                    caminhosPossivels.add(new Coordenada(coordenadaAtualVeiculo.getX() - 1, coordenadaAtualVeiculo.getY() - 1));

                }
                //verificando na frente do cruzamento a frente
                if ((malhaViaria.getCampos()[coordenadaAtualVeiculo.getX()][coordenadaAtualVeiculo.getY() - 2] != null)
                        && (malhaViaria.getCampos()[coordenadaAtualVeiculo.getX()][coordenadaAtualVeiculo.getY() - 2].getDirecao().equalsIgnoreCase("cima"))) {

                    caminhosPossivels.add((new Coordenada(coordenadaAtualVeiculo.getX(), coordenadaAtualVeiculo.getY() - 2)));

                }
                //verificando na direita do cruzamento a frente
                if ((malhaViaria.getCampos()[coordenadaAtualVeiculo.getX() + 1][coordenadaAtualVeiculo.getY() - 1] != null)
                        && (malhaViaria.getCampos()[coordenadaAtualVeiculo.getX() + 1][coordenadaAtualVeiculo.getY() - 1].getDirecao().equalsIgnoreCase("direita"))) {

                    caminhosPossivels.add((new Coordenada(coordenadaAtualVeiculo.getX() + 1, coordenadaAtualVeiculo.getY() - 1)));

                }

                break;

            case "baixo":

                //verificando na esquerda do cruzamento a frente
                if ((malhaViaria.getCampos()[coordenadaAtualVeiculo.getX() - 1][coordenadaAtualVeiculo.getY() + 1] != null)
                        && (malhaViaria.getCampos()[coordenadaAtualVeiculo.getX() - 1][coordenadaAtualVeiculo.getY() + 1].getDirecao().equalsIgnoreCase("esquerda"))) {

                    caminhosPossivels.add(new Coordenada(coordenadaAtualVeiculo.getX() - 1, coordenadaAtualVeiculo.getY() + 1));

                }
                //verificando na frente do cruzamento a frente
                if ((malhaViaria.getCampos()[coordenadaAtualVeiculo.getX()][coordenadaAtualVeiculo.getY() + 2] != null)
                        && (malhaViaria.getCampos()[coordenadaAtualVeiculo.getX()][coordenadaAtualVeiculo.getY() + 2].getDirecao().equalsIgnoreCase("baixo"))) {

                    caminhosPossivels.add((new Coordenada(coordenadaAtualVeiculo.getX(), coordenadaAtualVeiculo.getY() + 2)));

                }
                //verificando na direita do cruzamento a frente
                if ((malhaViaria.getCampos()[coordenadaAtualVeiculo.getX() + 1][coordenadaAtualVeiculo.getY() + 1] != null)
                        && (malhaViaria.getCampos()[coordenadaAtualVeiculo.getX() + 1][coordenadaAtualVeiculo.getY() + 1].getDirecao().equalsIgnoreCase("direita"))) {

                    caminhosPossivels.add((new Coordenada(coordenadaAtualVeiculo.getX() + 1, coordenadaAtualVeiculo.getY() + 1)));

                }

                break;

        }

        return caminhosPossivels;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Campo getCampo() {
        return campo;
    }

    public void setCampo(Campo campo) {
        this.campo = campo;
    }

}
